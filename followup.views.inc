<?php

/**
 * Implementation of hook_views_data().
 */
function followup_views_data() {
  $data = array(
    'followup' => array(
      'table' => array(
        'group' => t('Follow-up'),
        'base' => array(
          'field' => 'name',
          'title' => t('Follow-up'),
          'help' => t('List of configured follow-ups'),
        ),
      ),
      'name' => array(
        'title' => t('Name'),
        'help' => t('The unique identifier of this follow-up configuration.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_equality',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'description' => array(
        'title' => t('Description'),
        'help' => t('Description for this follow-up configuration.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'trigger_type' => array(
        'title' => t('Trigger type'),
        'help' => t('The node type that will trigger this follow-up.'),
        'field' => array(
          'handler' => 'views_handler_field_node_type',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_node_type',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_node_type',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'trigger_date_field' => array(
        'title' => t('Trigger date field'),
        'help' => t('Date field used to calculate the schedule for this follow-up.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'target_type' => array(
        'title' => t('Target type'),
        'help' => t('The node type of the follow-up nodes.'),
        'field' => array(
          'handler' => 'views_handler_field_node_type',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_node_type',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_node_type',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'target_reference_field' => array(
        'title' => t('Target reference field'),
        'help' => t('Nodereference field used to link the resulting node to its follow-up trigger.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'target_requires_trigger' => array(
        'title' => t('Target requires trigger'),
        'help' => t('Whether the follow-up node can only be created from a follow-up trigger.'),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_boolean_operator',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'followup_frequency' => array(
        'title' => t('Frequency'),
        'help' => t(''),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'followup_interval' => array(
        'title' => t('Interval'),
        'help' => t('The number of follow-ups that are scheduled.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'followup_grace_period' => array(
        'title' => t('Grace period'),
        'help' => t(''),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'followup_prevent_gaps' => array(
        'title' => t('Prevent gaps'),
        'help' => t(''),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_boolean_operator',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'status' => array(
        'title' => t('Status'),
        'help' => t(''),
        'field' => array(
          'handler' => 'views_handler_field_boolean',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_boolean_operator',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
    ),
    'followup_record' => array(
      'table' => array(
        'group' => t('Follow-up record'),
        'join' => array(
          'node' => array(
            'left_field' => 'nid',
            'field' => 'nid',
          ),
          'followup' => array(
            'left_field' => 'name',
            'field' => 'followup',
          ),
        ),
      ),
      'nid' => array(
        'title' => t('Trigger node'),
        'help' => t('The node that triggered this follow-up.'),
        'relationship' => array(
          'handler' => 'views_handler_relationship',
          'base' => 'node',
          'base field' => 'nid',
          'label' => t('Trigger node'),
        ),
      ),
      'sequence' => array(
        'title' => t('Sequence'),
        'help' => t('Identifier for individual follow-up of a specific node.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
          'numeric' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'followup' => array(
        'title' => t('Follow-up'),
        'help' => t('Identifier for follow-up to which this record belongs.'),
        'relationship' => array(
          'handler' => 'views_handler_relationship',
          'base' => 'followup',
          'base field' => 'name',
          'label' => t('Follow-up'),
        ),
      ),
      'target_date' => array(
        'title' => t('Target date'),
        'help' => t('Target date for this follow-up.'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
      'target_nid' => array(
        'title' => t('Target node'),
        'help' => t('The node that was created as a result of this follow-up.'),
        'relationship' => array(
          'handler' => 'views_handler_relationship',
          'base' => 'node',
          'base field' => 'nid',
          'label' => t('Target node'),
        ),
      ),
      'status' => array(
        'title' => t('Status'),
        'help' => t('The status of this follow-up.'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_equality',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
    ),
  );
  return $data;
}
