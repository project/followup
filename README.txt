
Summary
=======
Follow-up allows you to schedule the creation of nodes, triggered by the
creation of a node. That is, it allows you to configure a trigger node type
with information on the target nodes and the follow-up schedule, and the module
will create a schedule for when the target nodes should be created.

As an example, you may have a questionnaire on which you want to collect
additional responses from the same users every month for the next three months.
The follow-up module will show a table when each next node is due. The
follow-up nodes will, optionally, have a node reference back to the node that
triggered the schedule.

Requirements
============
This module depends on CCK and Date modules.
* http://drupal.org/project/cck
* http://drupal.org/project/date

Configuration
=============
Once installed, go to ?q=admin/build/followup and click on Add. You will be
presented with all the options for configure a follow-up schedule. Once you
Save it, it will show up in the table.

You can have as many concurrent follow-up schedules as you want, and each of
them will be triggered when a node of the corresponding trigger node type is
created.

To Use
======
Once a follow-up schedule is configured, each follow-up-enabled node will have
a new Follow-up tab, accessible from q?=node/123/followup, from where the user
is able to add the follow-up nodes.

The module also has Views integration, so that you can create a custom table
for displaying the scheduled follow-ups.

Credits
=======
Author:
* Victor Kareh (vkareh) - http://www.vkareh.net

Original concept:
* Bruce Rogers
* Brian Shensky (bshensky) - http://www.struction.com

Sponsored by:
* University of Michigan Cardiovascular Center - http://www.med.umich.edu/CVC-Research/areas/healthservices/mcorrp/index.shtml
